package se.experis.com.noticeboard.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.com.noticeboard.model.Notice;

public interface NoticeRepository extends JpaRepository<Notice, Integer> {
    Notice getById(Integer id);
}
