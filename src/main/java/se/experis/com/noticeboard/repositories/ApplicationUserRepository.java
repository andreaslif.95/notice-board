package se.experis.com.noticeboard.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.com.noticeboard.model.ApplicationUser;
import se.experis.com.noticeboard.model.Notice;

public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, Integer> {
    ApplicationUser getById(Integer id);
}
