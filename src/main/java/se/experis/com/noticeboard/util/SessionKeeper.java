package se.experis.com.noticeboard.util;

import se.experis.com.noticeboard.model.Login;

import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

/**
 * Singelton which hols information about the sessions.
 */
public class SessionKeeper {
    public static SessionKeeper sessionKeeper = null;
    private HashMap<String, Login> validSessions;

    private SessionKeeper() {
        validSessions = new HashMap<>();
    }

    public static SessionKeeper getInstance() {
        if(sessionKeeper == null) {
            sessionKeeper = new SessionKeeper();
        }
        return sessionKeeper;
    }
    public void removeSession(String session) {
        validSessions.remove(session);
    }
    public Integer CheckSession(String session) {
        Login login = validSessions.get(session);
        //Session exists
        if(login != null){
            Date now = new Date();
            // Session has not expired (yet)
            if ((now.getTime() - login.lastLogin.getTime()) <= 900000){
                login.lastLogin = now;
                return login.userId;
            }
            else {
                validSessions.remove(session);
            }
        }
        return null;
    }

    /**
     * Returs the username of the user having the session.
     * @param session
     * @return - Username of the sesssion.
     */
    public String getSessionName(String session) {
        return validSessions.get(session) != null ? validSessions.get(session).name : "";
    }
    public String addSession(String session, Integer id, String name) {
        if(validSessions.get(session) != null) {
            validSessions.get(session).lastLogin = new Date();
            return session;
        }
        validSessions.put(session, new Login(id, name));
        return session;
    }
}
