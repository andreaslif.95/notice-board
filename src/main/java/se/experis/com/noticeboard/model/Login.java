package se.experis.com.noticeboard.model;

import java.util.Date;

/**
 * Helper class for the session keeper.
 */
public class Login {
    public Integer userId;
    public Date lastLogin;
    public String name;

    public Login(Integer userId, String name) {
        this.userId = userId;
        this.name = name;
        this.lastLogin = new Date();
    }
}
