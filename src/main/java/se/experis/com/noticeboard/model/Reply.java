package se.experis.com.noticeboard.model;

import javax.persistence.*;
import java.util.Date;
@Entity
public class Reply {
    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    public Integer id;

    @Column
    public String createdBy;

    @Column
    public Date created;

    @Column
    public String text;

    public Reply(String createdBy, Date created, String text) {
        this.createdBy = createdBy;
        this.created = created;
        this.text = text;
    }

    public Reply() {

    }
}
