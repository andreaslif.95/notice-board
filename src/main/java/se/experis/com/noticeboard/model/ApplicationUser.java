package se.experis.com.noticeboard.model;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
@Entity

@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class ApplicationUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(nullable = false)
    public String name;

    @JsonIgnore
    @Column(nullable = false)
    public String password;

    @JsonSetter
    public void setPassword(String password) {
        this.password = password;
    }

    @OneToMany(targetEntity=Notice.class, cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, orphanRemoval = true)
    public List<Notice> notices = new ArrayList<>();

    public boolean auth(ApplicationUser u) {
        return this.name.equals( u.name) && this.password.equals(u.password);
    }
}
