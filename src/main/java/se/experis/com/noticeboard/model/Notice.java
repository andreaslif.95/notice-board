package se.experis.com.noticeboard.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Notice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(nullable = false)
    public String title;

    @Column(nullable = false)
    public String createdBy;

    @Column
    public String url;

    @Column
    public Date lastEdited;

    @Column
    public int views;

    @Column
    public Date created;

    @Column(nullable = false, columnDefinition="TEXT")
    public String text;
    //Has many replies, replies doesn't know nything about this relationship.
    @OneToMany(targetEntity=Reply.class, cascade = CascadeType.ALL,
    fetch = FetchType.LAZY, orphanRemoval = true)
    public List<Reply> replies = new ArrayList<>();
}
