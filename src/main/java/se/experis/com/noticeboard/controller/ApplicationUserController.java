package se.experis.com.noticeboard.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.com.noticeboard.model.ApplicationUser;
import se.experis.com.noticeboard.model.CommonResponse;
import se.experis.com.noticeboard.model.Notice;
import se.experis.com.noticeboard.model.Reply;
import se.experis.com.noticeboard.repositories.ApplicationUserRepository;
import se.experis.com.noticeboard.repositories.NoticeRepository;
import se.experis.com.noticeboard.util.SessionKeeper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Date;

@RestController
public class ApplicationUserController {
    @Autowired
    private ApplicationUserRepository repository;

    /**
     * Loggs in the specific user and adds him/her to the sessionKeeper so that we know it later.
     * @param user - User info to check if it exists.
     * @param session
     * @return ResponseEntity<CommonResponse> - Containing cr {data: - data of the response -, message: information about the response}, http status.
     */
    @RequestMapping(value = "/login", method=RequestMethod.POST)
    public ResponseEntity<CommonResponse> loginUser(@RequestBody ApplicationUser user, HttpSession session) {
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;
        //Loops all users to see if someone exists.
        for(ApplicationUser u : repository.findAll()) {
            if(user.auth(u)) {
                SessionKeeper.getInstance().addSession(session.getId(), u.id, u.name);
                cr.data = u;
                cr.message = "Sucessfully authenticated";
                resp = HttpStatus.ACCEPTED;
                return new ResponseEntity<>(cr, resp);
            }
        }
        cr.data = null;
        cr.message = "Failed to authenticate";
        resp = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(cr, resp);
    }

    /**
     * Used to check info regarding the user.
     * @param id - Id of the user to be checked.
     * @return ResponseEntity<CommonResponse> - Containing cr {data: - data of the response -, message: information about the response}, http status.
     */
    @RequestMapping(value = "/checkUser/{id}", method=RequestMethod.GET)
    public ResponseEntity<CommonResponse> checkUser(@PathVariable Integer id) {
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;
        ApplicationUser user = repository.getById(id);
        if(user == null) {
            cr.data = null;
            cr.message = "User not found";
            resp = HttpStatus.NOT_FOUND;
        }
        else {
            cr.data = user;
            cr.message = "Sucessfully found user";
            resp = HttpStatus.ACCEPTED;
        }
        return new ResponseEntity<>(cr, resp);
    }

    /**
     * Registers a user.
     * @param user
     * @return
     */
    @RequestMapping(value = "/register", method=RequestMethod.POST)
    public ResponseEntity<CommonResponse> registerUser(@RequestBody ApplicationUser user) {
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;
        cr.data = repository.save(user);
        cr.message = "Sucessfully created user";
        resp = HttpStatus.ACCEPTED;

        return new ResponseEntity<>(cr, resp);
    }



}
