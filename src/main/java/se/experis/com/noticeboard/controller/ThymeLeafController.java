package se.experis.com.noticeboard.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import se.experis.com.noticeboard.model.Notice;
import se.experis.com.noticeboard.repositories.NoticeRepository;
import se.experis.com.noticeboard.util.SessionKeeper;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;

@Controller
public class ThymeLeafController {
    @Autowired
    private NoticeRepository repository;

    /**
     * Get mapping for the index page.
     * @param redirectedFrom - Object to indicate if we were redirected, if so we want to display a alert.
     * @param model
     * @param session
     * @return index.hmtl
     */
    @GetMapping("/")
    public String getIndex(@ModelAttribute("deleted") Object redirectedFrom, Model model, HttpSession session) {

        ArrayList<Notice> notices = (ArrayList<Notice>) repository.findAll();
        notices.sort((o1, o2) -> o2.created.compareTo(o1.created));
        model = setAttributes(model, session, null, false);
        model.addAttribute("notices", notices);
        model.addAttribute("deleted", redirectedFrom instanceof Boolean);

        return "index";
    }

    /**
     * Gets the page to view of a speicif notice by id.
     * @param redirectedFrom - Object to indicate if we were redirected, if so we want to display a alert.
     * @param id - id of the notice to view.
     * @param model
     * @param session
     * @return notice.html
     */
    @RequestMapping(value = "/notice/{id}", method = RequestMethod.GET)
    public String getNotice(@ModelAttribute("message") String redirectedFrom, @PathVariable int id, Model model, HttpSession session) {
        Notice notice = repository.getById(id);
        if(notice != null) {
            notice.views++;
            notice = repository.save(notice);
            notice.replies.sort((o1, o2) -> o2.created.compareTo(o1.created));
        }
        //Adds info on if we should render edit buttons.
        model = setAttributes(model, session, notice, true);
        model.addAttribute("notice", notice);
        System.out.println(redirectedFrom);
        if(!redirectedFrom.equals("")) {
            System.out.println("true");
            model.addAttribute("message", redirectedFrom);
        }
        else {
            model.addAttribute("message", "");
        }
        return "notice";
    }

    /**
     * Renders a page to create a new notice. If not logged in user gets redirected to index.html
     * @param session
     * @return new.html
     */
    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String newNotice(HttpSession session) {
        if(SessionKeeper.getInstance().CheckSession(session.getId()) !=null) {
            return "new";
        }
        return "redirect:/";
    }

    /**
     * Page to view a specific notice which should be edited.
     * If the user isn't the owner of the notice which is requested he/she get redirected to the index.
     * @param id - Id of the notice to be edited.
     * @param model
     * @param session
     * @return edit.html
     */
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String editNotice(@PathVariable int id, Model model, HttpSession session) {
        Notice notice = repository.getById(id);
        if(notice != null && SessionKeeper.getInstance().CheckSession(session.getId()) !=null && SessionKeeper.getInstance().getSessionName(session.getId()).equals(notice.createdBy)) {
            model.addAttribute("notice", notice);
            return "edit";
        }
        return "redirect:/";
    }

    /**
     * Used to remove ugly urls. Herre we use this redirect to add information to the requests when we redirect.
     * @param success - type of redirect DELETED,SUCESS,CREATED
     * @param id - id of the notice to view after redirection.
     * @param attributes
     * @return
     */
    @RequestMapping(value = {"/redirectIndex/{success}/", "/redirectIndex/{success}/{id}"}, method = RequestMethod.GET)
    public RedirectView redirectWithUsingRedirectIndex(@PathVariable String success, @PathVariable(required = false) Integer id, RedirectAttributes attributes) {
        if(success == "deleted" || id == null) {
            attributes.addFlashAttribute("deleted", true);
            return new RedirectView("/");
        }
        else {
            attributes.addFlashAttribute("message", success);
        }
        return new RedirectView("/notice/"+id);
    }

    /**
     * Logs out the user.
     * @param session
     * @return
     */
    @RequestMapping(value = "/logout", method=RequestMethod.GET)
    public String registerUser(HttpSession session) {
        SessionKeeper.getInstance().removeSession(session.getId());

        return "redirect:/";
    }

    private Model setAttributes(Model model, HttpSession session, Notice notice, boolean checkOwner) {
        model.addAttribute("loggedIn", SessionKeeper.getInstance().CheckSession(session.getId()) != null);
        if(checkOwner) {
            if(notice != null) {
                model.addAttribute("owner", SessionKeeper.getInstance().getSessionName(session.getId()).equals(notice.createdBy));
            }
            else {
                model.addAttribute("owner", false);
            }
        }
        return model;
    }

}
