package se.experis.com.noticeboard.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.com.noticeboard.model.ApplicationUser;
import se.experis.com.noticeboard.model.CommonResponse;
import se.experis.com.noticeboard.model.Notice;
import se.experis.com.noticeboard.model.Reply;
import se.experis.com.noticeboard.repositories.ApplicationUserRepository;
import se.experis.com.noticeboard.repositories.NoticeRepository;
import se.experis.com.noticeboard.util.SessionKeeper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Date;

@RestController
public class NoticeController {
    @Autowired
    private NoticeRepository noticeRepository;
    @Autowired
    private ApplicationUserRepository userRepository;

    /**
     * Creates a new reply using patch since we actually edit our notices.
     * Requiered to be logged in.
     * @param id - id of the notice
     * @param reply - The reply object
     * @param session - Session.
     * @return ResponseEntity<CommonResponse> - Containing cr {data: - data of the response -, message: information about the response}, http status.
     */
    @RequestMapping(value = "/notice/reply/{id}", method=RequestMethod.PATCH)
    public ResponseEntity<CommonResponse> addReply(@PathVariable Integer id, @RequestBody Reply reply, HttpSession session){
        Notice notice = noticeRepository.getById(id);
        ResponseEntity<CommonResponse> responseEntity = checkAuth(session, "add a reply", notice, false);
        if(responseEntity != null) {
            return responseEntity;
        }

        CommonResponse cr = new CommonResponse();
        reply.created = new Date();
        reply.createdBy = SessionKeeper.getInstance().getSessionName(session.getId());
        notice.replies.add(reply);
        cr.data = noticeRepository.save(notice);
        cr.message = "Comment was created.";
        HttpStatus resp = HttpStatus.CREATED;

        return new ResponseEntity<>(cr, resp);
    }

    /**
     * Updating a specific notice
     * Requiered to be logged in and to be the owner of th enotice.
     * @param notice - Notice object
     * @param session - session.
     * @return ResponseEntity<CommonResponse> - Containing cr {data: - data of the response -, message: information about the response}, http status.
     */
    @RequestMapping(value = "/notice/update", method=RequestMethod.PATCH)
    public ResponseEntity<CommonResponse> updateNotice(@RequestBody Notice notice, HttpSession session){
        ResponseEntity<CommonResponse> responseEntity = checkAuth(session, "add update notice", notice, true);
        if(responseEntity != null) {
            return responseEntity;
        }

        CommonResponse cr = new CommonResponse();
        notice.lastEdited = new Date();
        cr.data = noticeRepository.save(notice);
        cr.message = "Sucessfully update";
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(cr, resp);
    }
    /**
     * Creates a random notice with a random date.
     * @param notice - Notice object
     * @param session - session.
     * @return ResponseEntity<CommonResponse> - Containing cr {data: - data of the response -, message: information about the response}, http status.
     */
    @PostMapping("/addWithRandomDate")
    public ResponseEntity<CommonResponse>  randomDateNotice(HttpSession session, @RequestBody Notice notice){
        //
        if(notice.url == null) {
            notice.url = "";
        }

        CommonResponse cr = new CommonResponse();
        ApplicationUser user = userRepository.getById(1);
        if(user == null) {
            return null;
        }

        notice.createdBy = user.name;
        user.notices.add(notice);
        userRepository.save(user);

        cr.data = user.notices.get(user.notices.size()-1);
        cr.message = "Notice was sucessfully created";
        HttpStatus resp = HttpStatus.CREATED;
        return new ResponseEntity<>(cr, resp);
    }

    /**
     * Creates a new notice.
     * Requieres a user to be logged in.
     * @param session - Session.
     * @param notice - The notice to be created.
     * @return
     */
    @PostMapping("/notice")
    public ResponseEntity<CommonResponse>  addNotice(HttpSession session, @RequestBody Notice notice){
        ResponseEntity<CommonResponse> responseEntity = checkAuth(session, "add update notice", notice, false);
        if(responseEntity != null) {
            return responseEntity;
        }


        if(notice.url == null) {
            notice.url = "";
        }

        CommonResponse cr = new CommonResponse();
        ApplicationUser user = userRepository.getById(SessionKeeper.getInstance().CheckSession(session.getId()));
        notice.created = new Date();
        notice.createdBy = user.name;
        user.notices.add(notice);
        userRepository.save(user);

        cr.data = user.notices.get(user.notices.size()-1);
        cr.message = "Notice was sucessfully created";
        HttpStatus resp = HttpStatus.CREATED;

        return new ResponseEntity<>(cr, resp);
    }

    /**
     * Deletes a specific notice.
     * Requieres a user to be logged in an own the notice.
     * @param session
     * @param id
     * @return ResponseEntity<CommonResponse> - Containing cr {data: - data of the response -, message: information about the response}, http status.
     */
    @DeleteMapping("/notice/{id}")
    public ResponseEntity<CommonResponse> removeNotice(HttpSession session, @PathVariable Integer id){
        ResponseEntity<CommonResponse> responseEntity = checkAuth(session, "add update notice", null, false);
        if(responseEntity != null) {
            return responseEntity;
        }

        CommonResponse cr = new CommonResponse();
        HttpStatus resp;
        Notice find = null;
        ApplicationUser user = userRepository.getById(SessionKeeper.getInstance().CheckSession(session.getId()));
        //Loops through all the users notices and tries to find the actual notice.
        for(Notice notice : user.notices) {
            System.out.println(notice.id);
            if(notice.id.equals(id)) {
                find = notice;
                break;
            }
        }

        if(find == null) {
            cr.data = null;
            cr.message = "The requested notice did not exist.";
            resp = HttpStatus.NOT_FOUND;
        }
        else {
            //Removes the notice by removing it from the user and updating it.
            cr.data = null;
            cr.message = "Sucessfully deleted";
            user.notices.remove(find);
            userRepository.save(user);
            resp = HttpStatus.OK;
        }

        return new ResponseEntity<>(cr, resp);
    }

    /**
     * Helper to check that the requester is authenticated
     * @param session - Session
     * @param message - Message to be sent
     * @param notice - Notice to controll ownership against
     * @param checkOwnership - If we should controll ownership
     * @return
     */
    private ResponseEntity<CommonResponse> checkAuth(HttpSession session, String message, Notice notice, boolean checkOwnership) {
        Integer id = SessionKeeper.getInstance().CheckSession(session.getId());
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;
        if(id == null) {
            cr.data = null;
            cr.message = "You need to be signed in to " + message;
            resp = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(cr, resp);
        }
        ApplicationUser user = userRepository.getById(id);
        if(user == null) {
            cr.data = null;
            cr.message = "You need to be signed in to " + message;
            resp = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(cr, resp);
        }
        if(checkOwnership) {
            if (notice == null) {
                cr.data = null;
                cr.message = "The requested notice does not exist";
                resp = HttpStatus.NOT_FOUND;
                return new ResponseEntity<>(cr, resp);
            }
            if (!notice.createdBy.equals(SessionKeeper.getInstance().getSessionName(session.getId()))) {
                cr.data = null;
                cr.message = "You dont own the resource.";
                resp = HttpStatus.BAD_REQUEST;
                return new ResponseEntity<>(cr, resp);
            }
        }
        return null;
    }

}